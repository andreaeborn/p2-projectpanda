import matplotlib.pyplot as plt
import xlsxwriter
import numpy as np
import sys
import glob
import os

filenames = sorted(glob.glob('spectra/Sp15*'))

print(filenames)

for f in filenames:
        print(f)
        file_name = [i for i in f.split('-')]
        path = 'excelspectra/{}.xlsx'.format(file_name[-1])
        data = np.loadtxt(f)
        x, y = data[:, 0], data[:, 1]
        plt.plot(x,y)
        plt.show()
        workbook = xlsxwriter.Workbook(filename=path)
        worksheet = workbook.add_worksheet()

        for i, (x, y) in enumerate(zip(x, y)):
                worksheet.write(i, 0, x)
                worksheet.write(i, 1, y)

        workbook.close()


