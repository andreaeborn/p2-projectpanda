import sys
import matplotlib.pyplot as plt
import pandas as pd
import glob
import xlsxwriter
from io import StringIO

bending = sys.argv[1] if len(sys.argv) > 1 else '.'
filenames = sorted(glob.glob('bending/Sp15*'))

for i,j in enumerate(filenames):
	print(j)
	file_name = [i for i in j.split('-')]
	path = file_name[-1].split('.')[0]
	filename = open(j)
	lines = filename.read().split('"Specimen"')
	print(filename)
	
	for e in range (1, len(lines)):
		df = pd.read_csv(StringIO(lines[e]), sep=",", skiprows=18)
		x= df['Strain [Exten.] %']
		y= df['Stress MPa']
		df2 = pd.DataFrame(x,y)
		plt.plot(x,y)
		plt.ylabel('Stress MPa')
		plt.xlabel('Strain [Exten.] %')
		plt.show()
		fn= 'excelbending/pd_{}_{}.xlsx'.format(path,e)
		df2.to_excel(fn)
	
